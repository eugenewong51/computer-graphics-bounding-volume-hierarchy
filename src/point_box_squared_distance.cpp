#include "point_box_squared_distance.h"
#include <iostream>

double point_box_squared_distance(
  const Eigen::RowVector3d & query,
  const BoundingBox & box)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here
  Eigen::RowVector3d p;
  double d;
  for (int i = 0; i < 3; i++) {
    if (query[i] > box.max_corner[i] ){
      p[i] = query[i] - box.max_corner[i];
    }else if (query[i] < box.min_corner[i]){
      p[i] = box.min_corner[i] - query[i];
    }else{
      p[i] = 0;
    }
  }
  d = p.squaredNorm();
  return d;
  ////////////////////////////////////////////////////////////////////////////
}
