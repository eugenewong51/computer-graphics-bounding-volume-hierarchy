#include "nearest_neighbor_brute_force.h"
#include <limits>// std::numeric_limits<double>::infinity();

void nearest_neighbor_brute_force(
  const Eigen::MatrixXd & points,
  const Eigen::RowVector3d & query,
  int & I,
  double & sqrD)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:
  I = -1;
  sqrD = 0;

  double d = std::numeric_limits<double>::infinity();
  int idx = -1;
  double d_i;
  for (int i = 0; i < points.rows(); i++) {
    d_i = (points.row(i) - query).squaredNorm();
    if (d_i<d){
      d = d_i;
      idx = i;
    }
  }
  if (idx!=-1){
    I = idx;
    sqrD = d;
  }
  ////////////////////////////////////////////////////////////////////////////
}
