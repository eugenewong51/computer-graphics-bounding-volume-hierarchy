#include "ray_intersect_triangle.h"
#include <Eigen/Geometry>

bool ray_intersect_triangle(
  const Ray & ray,
  const Eigen::RowVector3d & A,
  const Eigen::RowVector3d & B,
  const Eigen::RowVector3d & C,
  const double min_t,
  const double max_t,
  double & t)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:
  // from a2
  Eigen::Vector3d cA = A.transpose();
  Eigen::Vector3d cB = B.transpose();
  Eigen::Vector3d cC = C.transpose();

  Eigen::Vector3d e1 = cB-cA;
  Eigen::Vector3d e2 = cC-cA;
  
  //|[A,B,C]| = dot(A,cross(B,C))
  Eigen::Vector3d normal = e1.cross(e2);
  double det_A = -ray.direction.dot(normal);
  if (det_A != 0){
    
    Eigen::Vector3d oa = ray.origin - cA;
    Eigen::Vector3d oad_cross = oa.cross(ray.direction);
    double u = e2.dot(oad_cross)/det_A;
    if (u<0){
      return false;
    }
    double v = -1 * e1.dot(oad_cross)/det_A;
    
    if (v<0){
      return false;
    }
    
    if (u+v>1){
      return false;
    }
    
    double t_temp = oa.dot(normal)/det_A;
    if (t_temp<min_t || t_temp>max_t){
      return false;
    }
    t = t_temp;
    return true;
  }

  return false;
  ////////////////////////////////////////////////////////////////////////////
}

