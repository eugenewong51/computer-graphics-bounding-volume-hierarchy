#include "AABBTree.h"
#include <iostream>

// See AABBTree.h for API
bool AABBTree::ray_intersect(
  const Ray& ray,
  const double min_t,
  const double max_t,
  double & t,
  std::shared_ptr<Object> & descendant) const 
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:
  // 12.3.2
  t = std::numeric_limits<double>::infinity();
  if (ray_intersect_box(ray, box, min_t, max_t)){
    std::shared_ptr<Object> ld, rd, d;
    double lt, rt;
    bool l_intersect = this->left && this->left->ray_intersect(ray, min_t, max_t, lt, ld);
    bool r_intersect = this->right && this->right->ray_intersect(ray, min_t, max_t, rt, rd);

    if (l_intersect){
      t=lt;
      if (!ld){
        d=this->left;//left is leaf
      } else {
        d=ld;
      }
    }
    if (r_intersect && (!l_intersect || lt>rt)){
      t=rt;
      d=rd;
      if (!rd){
        d=this->right;//right is leaf
      } else {
        d=rd;
      }
    }
    if (r_intersect || l_intersect){
      descendant = d;
      return true;
    }
  }
  return false;
/////////////////////////////////////////////////////////////////////
}

