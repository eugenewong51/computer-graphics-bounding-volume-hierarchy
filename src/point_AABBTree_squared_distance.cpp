#include "point_AABBTree_squared_distance.h"
#include <queue> // std::priority_queue
#include <iostream>

bool point_AABBTree_squared_distance(
    const Eigen::RowVector3d & query,
    const std::shared_ptr<AABBTree> & root,
    const double min_sqrd,
    const double max_sqrd,
    double & sqrd,
    std::shared_ptr<Object> & descendant)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here
  std::priority_queue<std::pair<double, std::shared_ptr<Object>>, std::vector<std::pair<double, std::shared_ptr<Object>>>,
    std::greater<std::pair<double, std::shared_ptr<Object>>>> Q;
  double droot = point_box_squared_distance(query, root->box);
  Q.push(std::make_pair(droot, root));
  
  double d = std::numeric_limits<double>::infinity();
  std::shared_ptr<Object> desc;

  while (!Q.empty()){
    double ds = Q.top().first;
    std::shared_ptr<Object> subtree = Q.top().second;
    std::shared_ptr<AABBTree> st = std::dynamic_pointer_cast<AABBTree>(subtree);
    Q.pop();

    if (ds<d){
      double di;
      std::shared_ptr<Object> desci;
      if (!st){
        subtree->point_squared_distance(query, min_sqrd, max_sqrd, di, desci);
        if (di < d){
          d = di;
          desc = subtree;
        }
      } else {
        if (st->left){
          double dl = point_box_squared_distance(query, st->left->box);
          Q.push(std::make_pair(dl, st->left));
        }
        if (st->right){
          double dr = point_box_squared_distance(query, st->right->box);
          Q.push(std::make_pair(dr, st->right));
        }
      }
    }
  }
  sqrd = d;
  descendant = desc;
  return  d!= std::numeric_limits<double>::infinity();
  ////////////////////////////////////////////////////////////////////////////
}
