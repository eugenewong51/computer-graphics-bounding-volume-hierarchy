#include "find_all_intersecting_pairs_using_AABBTrees.h"
#include "box_box_intersect.h"
// Hint: use a list as a queue
#include <list>
#include <iostream>

void find_all_intersecting_pairs_using_AABBTrees(
  const std::shared_ptr<AABBTree> & rootA,
  const std::shared_ptr<AABBTree> & rootB,
  std::vector<std::pair<std::shared_ptr<Object>,std::shared_ptr<Object> > > & 
    leaf_pairs)
{
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  std::list<std::pair<std::shared_ptr<Object>, std::shared_ptr<Object>>> Q;
  if (box_box_intersect(rootA->box, rootB->box)){
    Q.push_back(std::make_pair(rootA, rootB));
  }
  while (!Q.empty()){
    std::shared_ptr<Object> nodeA = Q.front().first;
    std::shared_ptr<Object> nodeB = Q.front().second;
    Q.pop_front();
    std::shared_ptr<AABBTree> subtreeA = std::dynamic_pointer_cast<AABBTree>(nodeA);
    std::shared_ptr<AABBTree> subtreeB = std::dynamic_pointer_cast<AABBTree>(nodeB);

    if (!subtreeA && !subtreeB){
      leaf_pairs.push_back(std::make_pair(nodeA, nodeB));
    } else if (!subtreeA){
      if (subtreeB->left && box_box_intersect(nodeA->box, subtreeB->left->box)){
        Q.push_back(std::make_pair(nodeA, subtreeB->left));
      }
      if (subtreeB->right && box_box_intersect(nodeA->box, subtreeB->right->box)){
        Q.push_back(std::make_pair(nodeA, subtreeB->right));
      }
    }else if (!subtreeB){
      if (subtreeA->left && box_box_intersect(nodeB->box, subtreeA->left->box)){
        Q.push_back(std::make_pair(subtreeA->left, nodeB));
      }
      if (subtreeA->right && box_box_intersect(nodeB->box, subtreeA->right->box)){
        Q.push_back(std::make_pair(subtreeA->right, nodeB));
      }
    } else {
      if (subtreeA->left && subtreeB->left && box_box_intersect(subtreeA->left->box, subtreeB->left->box)){
        Q.push_back(std::make_pair(subtreeA->left, subtreeB->left));
      }
      if (subtreeA->left && subtreeB->right && box_box_intersect(subtreeA->left->box, subtreeB->right->box)){
        Q.push_back(std::make_pair(subtreeA->left, subtreeB->right));
      }
      if (subtreeA->right && subtreeB->right && box_box_intersect(subtreeA->right->box, subtreeB->right->box)){
        Q.push_back(std::make_pair(subtreeA->right, subtreeB->right));
      }
      if (subtreeA->right && subtreeB->left && box_box_intersect(subtreeA->right->box, subtreeB->left->box)){
        Q.push_back(std::make_pair(subtreeA->right, subtreeB->left));
      }
    }

  }
  ////////////////////////////////////////////////////////////////////////////
}
