#include "AABBTree.h"
#include "insert_box_into_box.h"
#include <iostream>

AABBTree::AABBTree(
  const std::vector<std::shared_ptr<Object> > & objects,
  int a_depth): 
  depth(std::move(a_depth)), 
  num_leaves(objects.size())
{
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  // textbook 12.3.2
  int N = objects.size();
  for (int i=0; i<N; i++){
    insert_box_into_box(objects[i]->box, box);
  }
  if (N<=0){
    return;
  } else if (N==1){
    this->left = objects[0];//leaf
    this->right = NULL;
  } else if(N==2){
    this->left = objects[0];//leaf
    this->right = objects[1];//leaf
  } else {
    int axis = -1;
    double length = 0;
    Eigen::RowVector3d len_i = box.max_corner - box.min_corner;
    Eigen::RowVector3d cen_i = box.center();
    for (int i = 0; i < 3; i++) {
      if (len_i[i]>=length){
        length = len_i[i];
        axis = i;
      }
    }
    double center = cen_i[axis];
    std::vector<std::shared_ptr<Object>> l_objects, r_objects;
    for (int i=0; i<N; i++){
      if (objects[i]->box.center()[axis] <= center){
        l_objects.push_back(objects[i]);
      } else{
        r_objects.push_back(objects[i]);
      }
    }
    
    if (l_objects.empty()){
      l_objects.push_back(r_objects.back());
      r_objects.pop_back();
    } else if (r_objects.empty()){
      r_objects.push_back(l_objects.back());
      l_objects.pop_back();
    }
    this->left = std::make_shared<AABBTree>(l_objects, a_depth+1); //subtree
    this->right = std::make_shared<AABBTree>(r_objects, a_depth+1); //subtree
  }
  ////////////////////////////////////////////////////////////////////////////
}
