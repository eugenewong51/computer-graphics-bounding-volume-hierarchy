#include "box_box_intersect.h"
#include <iostream>
bool box_box_intersect(
  const BoundingBox & A,
  const BoundingBox & B)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:
  for (int i = 0; i < 3; i++) {
    if (A.max_corner[i] < B.min_corner[i]){
      return false;
    }
    if (B.max_corner[i] < A.min_corner[i]){
      return false;
    }
  }
  return true;
  ////////////////////////////////////////////////////////////////////////////
}

