#include "ray_intersect_box.h"
#include <iostream>

bool ray_intersect_box(
  const Ray & ray,
  const BoundingBox& box,
  const double min_t,
  const double max_t)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:
  // 12.3.1
  double xmax = box.max_corner[0];
  double xmin = box.min_corner[0];
  double ymax = box.max_corner[1];
  double ymin = box.min_corner[1];
  double zmax = box.max_corner[2];
  double zmin = box.min_corner[2];

  double xa = 1/ray.direction[0];
  double ya = 1/ray.direction[1];
  double za = 1/ray.direction[2];

  double xe = ray.origin[0];
  double ye = ray.origin[1];
  double ze = ray.origin[2];
  
  double txmax, txmin, tymax, tymin, tzmax, tzmin;

  if (xa>=0){
    txmin = (xmin-xe)*xa;
    txmax = (xmax-xe)*xa;
  } else{
    txmin = (xmax-xe)*xa;
    txmax = (xmin-xe)*xa;
  }
  if (ya>=0){
    tymin = (ymin-ye)*ya;
    tymax = (ymax-ye)*ya;
  } else{
    tymin = (ymax-ye)*ya;
    tymax = (ymin-ye)*ya;
  }
  if (za>=0){
    tzmin = (zmin-ze)*za;
    tzmax = (zmax-ze)*za;
  } else{
    tzmin = (zmax-ze)*za;
    tzmax = (zmin-ze)*za;
  }
  double max = std::min(std::min(txmax, tymax), tzmax);
  double min = std::max(std::max(txmin, tymin), tzmin);
  if (min > max || min > max_t || max < min_t){
    return false;
  }
  return true;
  ////////////////////////////////////////////////////////////////////////////
}
