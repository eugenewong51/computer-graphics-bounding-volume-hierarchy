#include "ray_intersect_triangle_mesh_brute_force.h"
#include "ray_intersect_triangle.h"

bool ray_intersect_triangle_mesh_brute_force(
  const Ray & ray,
  const Eigen::MatrixXd & V,
  const Eigen::MatrixXi & F,
  const double min_t,
  const double max_t,
  double & hit_t,
  int & hit_f)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with you  
  double t = std::numeric_limits<double>::infinity();
  int f = -1;
  double t_i;
  for (int i = 0; i < F.rows(); i++) {
    Eigen::RowVector3d a = V.row(F(i, 0));
    Eigen::RowVector3d b = V.row(F(i, 1));
    Eigen::RowVector3d c = V.row(F(i, 2));
    bool intersect = ray_intersect_triangle(ray, a, b, c, min_t, max_t, t_i);
    if (intersect && (t_i < t)){
      f = i;
      t = t_i;
    }
  }
  hit_t = 0;
  hit_f = 0;
  if (f!=-1){
    hit_t = t;
    hit_f = f;
    return true;
  }
  return false;
  ////////////////////////////////////////////////////////////////////////////
}
