# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/AABBTree.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/AABBTree.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/AABBTree_ray_intersect.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/AABBTree_ray_intersect.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/box_box_intersect.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/box_box_intersect.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/find_all_intersecting_pairs_using_AABBTrees.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/find_all_intersecting_pairs_using_AABBTrees.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/insert_box_into_box.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/insert_box_into_box.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/insert_triangle_into_box.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/insert_triangle_into_box.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/nearest_neighbor_brute_force.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/nearest_neighbor_brute_force.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/point_AABBTree_squared_distance.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/point_AABBTree_squared_distance.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/point_box_squared_distance.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/point_box_squared_distance.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/ray_intersect_box.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/ray_intersect_box.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/ray_intersect_triangle.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/ray_intersect_triangle.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/ray_intersect_triangle_mesh_brute_force.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/ray_intersect_triangle_mesh_brute_force.cpp.o"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/src/triangle_triangle_intersection.cpp" "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/CMakeFiles/core.dir/src/triangle_triangle_intersection.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../libigl/external/glad/include"
  "../libigl/external/glfw/include"
  "../libigl/cmake/../include"
  "../libigl/cmake/../external/eigen"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/glad/CMakeFiles/glad.dir/DependInfo.cmake"
  "/mnt/c/users/eugen/Documents/GitHub/computer-graphics-bounding-volume-hierarchy/build-release/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
